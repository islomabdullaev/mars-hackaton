# Django
from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    TEACHER = 'teacher'
    STUDENT = 'student'
    TYPES = (
        (STUDENT, STUDENT),
        (TEACHER, TEACHER),
    )
    user_type = models.CharField(
        max_length=20, choices=TYPES,
        default=TYPES[0][0],
        blank=True, null=True
    )
    phone_number = models.CharField(max_length=13)

    USERNAME_FIELD = 'username'

    def __str__(self):
        return self.username
