# Models
from user.models import User

# Validations
from django.forms import ValidationError

# Django
from django.shortcuts import redirect


def signup(username: str, email: str, user_type: str, password, confirm_password):
    email_exists = 'email already exists'
    username_exists = 'username already exists'
    password_not_match = "password not matching"

    check_email = User.objects.filter(email=email).exists()
    if check_email is True:
        raise ValidationError(email_exists)
    check_username = User.objects.filter(username=username).exists()
    if check_email is True:
        raise ValidationError(username_exists)
    if password != confirm_password:
        raise ValidationError(password_not_match)
    user = create_user(username, email, user_type, password=password)
    return redirect('login')


def create_user(
        username: str,
        email: str,
        user_type: str,
        password=None
):
    user = User.objects.create(
        email=email,
        username=username,
        is_active=True,
        is_staff=False,
        is_superuser=False,
    )
    if password:
        user.set_password(password)
        user.save()
    return user
