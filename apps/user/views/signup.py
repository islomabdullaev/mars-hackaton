# Views
from django.views.generic import View

# Services
from user.services.signup import signup

# Django
from django.shortcuts import render


class SignupView(View):

    def get(self, request):
        return render(request, 'signup.html')

    def post(self, request):
        username = request.POST.get('username')
        email = request.POST.get('email')
        user_type = request.POST.get('user_type')
        password = request.POST.get('password')
        confirm_password = request.POST.get('confirm_password')
        return signup(username, email, user_type, password, confirm_password)
