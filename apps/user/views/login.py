# Views
from django.views.generic import View
from django.contrib.auth import authenticate, login
from django.http.response import HttpResponse
from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from django.urls import reverse_lazy


class IndexTemplateView(TemplateView):
    template_name = 'index.html'


class LoginView(View):

    def get(self, request):
        return render(request, 'login.html')

    def post(self, request):
        invalid_username_or_password = 'invalid_username_or_password'
        activate_error = 'please_activate_your_account'

        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active and user.user_type == 'student':
                login(request, user)
                return redirect('quiz-room-student')
            if user.user_type == 'teacher':
                return redirect('dashboard')
            else:
                return HttpResponse(activate_error)
        else:
            return HttpResponse(invalid_username_or_password)
