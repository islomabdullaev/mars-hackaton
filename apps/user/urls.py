# Django
from django.urls import path

# Views
from user.views.login import IndexTemplateView, LoginView
from user.views.logout import LogoutView
from user.views.signup import SignupView

urlpatterns = [
    path('signup/', SignupView.as_view(), name='signup'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('', IndexTemplateView.as_view(), name='index'),
]
