# Django
from django.urls import path

# Views
from quiz.views.create_quiz import CreateQuizView
from quiz.views.join_room import JoinRoom
from quiz.views.quiz import (
    QuizTeacherRoom,
    QuizStudentRoom,
    HomeTemplateView,
    QuizTemplateView, quiz_not_found
)
from quiz.views.quiz import quiz_found
from quiz.views.quiz_test import QuizQuestionsView

urlpatterns = [
    path('teacher/my-quizes/<int:pk>/', QuizTeacherRoom.as_view(), name='quiz-room-teacher'),
    path('teacher/dashboard/', HomeTemplateView.as_view(), name='dashboard'),
    path('teacher/my-quizes/', QuizTemplateView.as_view(), name='my-quizes'),
    path('student/', QuizStudentRoom.as_view(), name='quiz-room-student'),
    path('join-quiz-room/', JoinRoom.as_view(), name='join-quiz-room'),
    path('create-quiz/', CreateQuizView.as_view(), name='create-quiz'),
    path('quiz-test/<int:quiz_pk>/', QuizQuestionsView.as_view(), name='quiz-test'),
    path('quiz-found/', quiz_found, name='quiz-found'),
    path('quiz-not-found/', quiz_not_found, name='quiz-not-found'),
]
