# Models
import string

from django.db import models
from core.base_model import BaseModel
from point.models.point import Point
from user.models import User

# Django
from django.core.validators import MaxValueValidator, MinValueValidator

# Services
from quiz.services.get_random_code import random_string_generator, random_list

chars = string.digits + string.digits
size = 6


class Quiz(BaseModel):
    title = models.CharField(max_length=56)
    points = models.IntegerField()
    time_limit = models.IntegerField(validators=[MinValueValidator(30), MaxValueValidator(60)], default=30)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='quizes')
    users = models.ManyToManyField(User)
    question = models.CharField(max_length=56)

    choice1 = models.CharField(max_length=56)
    choice2 = models.CharField(max_length=56)
    choice3 = models.CharField(max_length=56)
    choice4 = models.CharField(max_length=56)

    answer = models.CharField(max_length=56)
    pin_code = models.CharField(max_length=6, default=random_string_generator(size, chars))

    def __str__(self):
        return f'Quiz: {self.title}'

    class Meta:
        ordering = ['created_datetime']
        verbose_name_plural = 'Quizes'
