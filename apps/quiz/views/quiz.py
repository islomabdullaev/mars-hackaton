# Django
from django.shortcuts import render

# Views
from django.views.generic import ListView, TemplateView
from django.views.generic import View

# Models
from quiz.models import Quiz


class HomeTemplateView(TemplateView):
    template_name = 'home.html'


class QuizTemplateView(ListView):
    template_name = 'quizes.html'
    model = Quiz
    context_object_name = 'my_quizes'

    def get_queryset(self):
        return Quiz.objects.filter(user=self.request.user)


class QuizTeacherRoom(View):

    def get(self, request, pk):
        quiz = Quiz.objects.get(pk=pk)
        return render(request, 'adduser.html', {'quiz': quiz})


class QuizStudentRoom(View):

    def get(self, request):
        return render(request, 'gamepin.html')


def quiz_found(request):
    return render(request, 'quiz_found.html')


def quiz_not_found(request):
    return render(request, 'quiz_not_found.html')
