# View
from django.views.generic import View

# Django
from django.shortcuts import render


class CreateQuizView(View):
    def get(self, request):
        return render(request, 'create.html')

    def post(self, request):
        pass