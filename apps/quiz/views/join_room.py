# Django
from django.views.generic import TemplateView
from django.views.generic import View
from django.shortcuts import HttpResponse, redirect

# Models
from quiz.models import Quiz


class JoinRoom(View):

    def post(self, request):
        pin_code = request.POST.get('pin_code')
        try:
            check_quiz_room = Quiz.objects.get(pin_code=pin_code)
            if check_quiz_room is not None:
                quiz = check_quiz_room.users.add(request.user)
                return redirect('quiz-found')
            else:
                return redirect('quiz-not-found')
        except Quiz.DoesNotExist:
            return redirect('quiz-not-found')



