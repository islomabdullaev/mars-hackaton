# View
from django.views.generic import View

# Django
from django.shortcuts import render

from quiz.models import Quiz


class QuizQuestionsView(View):
    def get(self, request, quiz_pk):
        quiz = Quiz.objects.filter(pk=quiz_pk)[0]
        return render(request, 'duringame.html', {"quiz": quiz})

    def post(self, request):
        answer = request.POST.get('answer')
