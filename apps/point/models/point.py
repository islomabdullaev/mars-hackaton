import string
# Models
from django.db import models
from core.base_model import BaseModel
from user.models.base import User

chars = string.ascii_letters + string.punctuation
size = 6


class Point(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.IntegerField(default=0)

    def __str__(self):
        return f'Quiz: {self.score}'

    class Meta:
        ordering = ['created_datetime']
        verbose_name_plural = 'Points'


class Answer(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_answer')
    true = models.BooleanField()
    false = models.BooleanField()

    def __str__(self):
        return f'Quiz: {self.user.username}'

    class Meta:
        ordering = ['created_datetime']
        verbose_name_plural = 'Answer'
